#!/usr/bin/env python3

def is_palindrome(input):
    if input == None:
        return False

    reversed = input[::-1]

    return reversed == input

def test_is_palindrome_with_none():
    assert not is_palindrome(None)

def test_is_palindrome_with_empty():
    assert is_palindrome("")

def test_is_palindrome_with_racecare():
    assert is_palindrome("racecar")

def test_is_palindrome_with_madam():
    assert is_palindrome("madam")

def test_is_palendorme_fails_with_my_name():
    assert not is_palindrome("shey")
